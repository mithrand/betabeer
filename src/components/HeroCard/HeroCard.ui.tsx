import React from 'react';
import './HeroCard.styles.scss';
import 'bootstrap/dist/css/bootstrap-grid.min.css';

export interface HeroCardProps {
  id: string;
  name: string;
  description: string;
  thumbnail: string;
  onClick(evt: React.MouseEvent<HTMLDivElement>):void
}

export const HeroCard = (props: HeroCardProps) =>
  <div className="heroCard" onClick={props.onClick}>
    <img className="heroCard__image" src={props.thumbnail} alt={props.name}></img>
    <div className="heroCard__content">
      <span className="heroCard__content__name">{props.name}</span>
      <span className="heroCard__content__description">{props.description}</span>
    </div>
  </div>
