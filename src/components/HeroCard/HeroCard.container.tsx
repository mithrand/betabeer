import React from 'react';
import { HeroCard } from './HeroCard.ui';
import { IHero } from '../../types';

export interface HeroCardContainerProps  extends IHero {
}

export const HeroCardContainer = (props: HeroCardContainerProps) => {
  
  const onClickHandler = (evt: React.MouseEvent) => {
    evt.preventDefault();
    window.alert(`Card clicked ${props.id}`);
  };

  return (
    <HeroCard
      id={props.id}
      name={props.name}
      description={props.description}
      thumbnail={props.thumbnail}
      onClick={onClickHandler}
    />
  );

};
