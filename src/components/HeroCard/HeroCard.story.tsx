import React from 'react';
import {
  HeroCardContainer,
  HeroCardContainerProps,
} from './HeroCard.container';

const heroCardContainerProps: HeroCardContainerProps = {
  id: '1009718',
  name: 'Wolverine',
  description:
    "Born with super-human senses and the power to heal from almost any wound, Wolverine was captured by a secret Canadian organization and given an unbreakable skeleton and claws. Treated like an animal, it took years for him to control himself. Now, he's a premiere member of both the X-Men and the Avengers.",
  thumbnail: 'http://i.annihil.us/u/prod/marvel/i/mg/2/60/537bcaef0f6cf.jpg',
};

export const WithHero = () => (
  <div className="heroCardWrapper" style={{width:300}}>
    <HeroCardContainer {...heroCardContainerProps} />
  </div>
);

export default { title: 'HeroCard' };
