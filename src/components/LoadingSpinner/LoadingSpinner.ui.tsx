import React from 'react';
import PacmanLoader from 'react-spinners/PacmanLoader';
import './LoadingSpinner.styles.scss';

const spinnerColor = '#e62429';

export const LoadingSpinner = () => (
  <div className="loadingSpinner">
    <PacmanLoader color={spinnerColor} />
  </div>
);
