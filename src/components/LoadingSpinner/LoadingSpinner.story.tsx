import React from 'react';
import { LoadingSpinner } from './LoadingSpinner.ui';

export const PacmanSpinner = () => <LoadingSpinner  />;

export default { title: 'LoadingSpinner' };
