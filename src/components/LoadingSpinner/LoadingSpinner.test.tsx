import React from 'react';
import { mount } from 'enzyme';

import { LoadingSpinner } from './LoadingSpinner.ui';

describe('LoadingSpinner', () => {

  it('LoadingSpinner renders', () => {
      const component = (<LoadingSpinner />);
      const wrapper = mount(component);
      expect(wrapper.html()).toMatchSnapshot();
  });

});