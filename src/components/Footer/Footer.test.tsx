import React from 'react';
import { mount } from 'enzyme';

import { Footer } from './Footer.ui';

describe('Footer', () => {

  it('Footer renders', () => {
      const component = (<Footer />);
      const wrapper = mount(component);
      expect(wrapper.html()).toMatchSnapshot();
  });

});