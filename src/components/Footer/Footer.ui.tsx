import React from 'react';

import './Footer.styles.scss';

export const Footer = () => 
    <div className="footer">
        <span className="footer__copy">©2019 MARVEL</span>
    </div>