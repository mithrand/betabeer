import React from 'react';
import { IHero } from '../../types';
import HeroCard from '../HeroCard';
import './HeroesList.styles.scss';

export interface HeroesListProps {
  heroes: IHero[];
}

export const HeroesList = (props: HeroesListProps) => (
  <div className="heroesList">
    <div className="row">
      {props.heroes.map(hero => (
          <div key={`heroListItem-${hero.id}`} className="col-12 col-sm-6 col-md-4 col-lg-3">
            <HeroCard  {...hero} />
          </div>
      ))}
    </div>
  </div>
);
