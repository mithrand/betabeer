import React from 'react';
import { mount } from 'enzyme';

import { Header } from './Header.ui';

describe('Header', () => {

  it('Header renders', () => {
      const component = (<Header />);
      const wrapper = mount(component);
      expect(wrapper.html()).toMatchSnapshot();
  });

});