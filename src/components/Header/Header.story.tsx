import React from 'react';

import {
  Header,
} from './Header.ui';

export const HeaderWithLogo = () => <Header />

export default { title: 'Header' };