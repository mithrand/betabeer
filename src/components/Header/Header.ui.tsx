import React from 'react';

import SearchBar from '../SearchBar';
import { Logo } from '../../Icons';
import './Header.styles.scss';

const triggerSearch = (heroName: string) =>
  window.alert(`Searching hero ${heroName}`);

export const Header = () => (
  <header className="header">
    <Logo className="header__logo" />
    <SearchBar triggerSearch={triggerSearch} />
  </header>
);
