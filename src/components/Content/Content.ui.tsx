import React from 'react';
import { IHero } from '../../types';
import './Content.style.scss';

import HeroesList from '../HeroesList';
import { Logo } from '../../Icons'
import LoadingSpinner from '../LoadingSpinner';


export interface ContentProps {
  heroes: IHero[];
  lodading: boolean;
}

export const Content = (props: ContentProps) => (
  <div className="content">
    <Logo className="content__logo" />
    { props.lodading ?  <LoadingSpinner /> : <HeroesList heroes={props.heroes} /> }
  </div>
);
