import React from 'react';
import { mount } from 'enzyme';

import { Content, ContentProps } from './Content.ui';

describe('Content', () => {

  const contentProps: ContentProps = {
    heroes: [
      {
        id: '1009718',
        name: 'Wolverine',
        description:
          "Born with super-human senses and the power to heal from almost any wound, Wolverine was captured by a secret Canadian organization and given an unbreakable skeleton and claws. Treated like an animal, it took years for him to control himself. Now, he's a premiere member of both the X-Men and the Avengers.",
        thumbnail:
          'http://i.annihil.us/u/prod/marvel/i/mg/2/60/537bcaef0f6cf.jpg',
      },
    ],
    lodading: false,
  };

  it('Content renders', () => {
    const component = <Content {...contentProps} />;
    const wrapper = mount(component);
    expect(wrapper.html()).toMatchSnapshot();
  });

  it('Content loading renders', () => {
    const component = <Content {...contentProps} lodading={true} />;
    const wrapper = mount(component);
    expect(wrapper.html()).toMatchSnapshot();
  });

});
