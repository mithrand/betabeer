import React from 'react';
import { mount } from 'enzyme';

import { SearchBar as SearchBarUI, PropsType as SearchBarUIProps } from './SearchBar.ui';


describe('searchBar - UI', () => {

  const defaultProps : SearchBarUIProps = {
    setActive:()=>{},
    setInnactive: () => {},
    value:'',
    isActive: false,
    reset:()=> {},
    onChange: ()=> {},
    onKeyPress: () => {},
  }

  it('SearchBar inactive and empty', () => {
      const component = ( <SearchBarUI {...defaultProps} />);
      const wrapper = mount(component);
      expect(wrapper.html()).toMatchSnapshot();
  });

  it('SearchBar Active and empty', () => {
      const component = ( <SearchBarUI {...defaultProps} isActive={true} />);
      const wrapper = mount(component);
      expect(wrapper.html()).toMatchSnapshot();
  });

  it('SearchBar Active and with text', () => {
    const component = ( <SearchBarUI {...defaultProps} isActive={true} value="SpiderMan"/>);
    const wrapper = mount(component);
    expect(wrapper.html()).toMatchSnapshot();
  });

  it('SearchBar innactive and with text', () => {
    const component = ( <SearchBarUI {...defaultProps} value="SpiderMan"/>);
    const wrapper = mount(component);
    expect(wrapper.html()).toMatchSnapshot();
  });

});
