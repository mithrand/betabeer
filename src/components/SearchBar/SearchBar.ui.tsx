import React from 'react';
import './SearchBar.styles.scss';

import { 
  Search as SearchIcon,
  Cancel as CancelIcon
} from '../../Icons';

export interface PropsType  {
  value: string
  isActive: boolean;
  setActive():void;
  setInnactive():void;
  reset():void;
  onChange(evt: React.ChangeEvent<HTMLInputElement>):void;
  onKeyPress(evt: React.KeyboardEvent):void;
}

export const SearchBar = (props: PropsType) => (
  <div className={`searchBar ${props.isActive ? 'searchBar--active' : ''}`} onFocus={props.setActive} onBlur={props.setInnactive}>
      <SearchIcon className="searchBar__icon" />
      <input 
        className="searchBar__input" 
        name="searchKeyword" 
        placeholder="Buscar un super heroe" 
        type="text"
        autoComplete="off"
        value={props.value}
        onChange={props.onChange}
        onKeyPress={props.onKeyPress}
      />
      { props.value && (
          <button className="searchBar__reset" onClick={props.reset}>
            <CancelIcon className="searchBar__reset__icon"/>
          </button>
        )
      }
  </div>
)