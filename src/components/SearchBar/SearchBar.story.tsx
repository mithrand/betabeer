import React from 'react';
import { SearchBar, PropsType as SearchBarProps } from './SearchBar.ui';
import { 
    SearchBarContainer, 
} from './SearchBar.container';

const defaultProps : SearchBarProps = {
    setActive:()=>{},
    setInnactive: () => {},
    value:'',
    isActive: false,
    reset:()=> {},
    onChange: ()=> {},
    onKeyPress: () => {},
}

const fakeTriggerSearch = (heroName: string) => window.alert(`Searching  ${heroName}...`)

export const Innactive = () => <SearchBar {...defaultProps} />
export const Active = () => <SearchBar {...defaultProps} isActive={true} />
export const InnactiveAndContent = () => <SearchBar {...defaultProps} value="SpiderMan" />
export const ActiveAndContent = () => <SearchBar {...defaultProps} isActive={true} value="SpiderMan" />

export const live = () => <SearchBarContainer triggerSearch={fakeTriggerSearch} />

export default { title: 'SearchBar' };