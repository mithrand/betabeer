import React, { Component } from 'react'
import { SearchBar } from './SearchBar.ui'

export interface PropsType {
    triggerSearch(heronNAme: string):void;
}
export interface StateType {
    isActive: boolean;
    heroName: string;
}

export class SearchBarContainer extends Component<PropsType, StateType> {
    
    state:StateType = {
        isActive: false,
        heroName: '',
    }

    setActive = () => this.setState({ isActive: true });
    
    setInnactive = () => this.setState({ isActive: false });

    onHeroNameChangeHandler = (evt: React.ChangeEvent<HTMLInputElement>) => this.setState({ heroName: evt.target.value })

    resetHeroName = () => this.setState({ heroName: ''})

    search = (evt: React.KeyboardEvent) => {
        const { heroName } = this.state;
        const { triggerSearch } = this.props;

        if(evt.key === 'Enter' && heroName){
            triggerSearch(heroName);
          }
    }

    render(){
        return (
            <SearchBar
                value={this.state.heroName} 
                isActive={this.state.isActive}
                setActive={this.setActive}
                setInnactive={this.setInnactive}
                onChange={this.onHeroNameChangeHandler}
                reset={this.resetHeroName}
                onKeyPress={this.search}
            />
        )
    }
}