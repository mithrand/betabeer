import React, { ReactElement } from 'react';
import { Cancel, Search, Logo } from './Icons';

const IconsListStyle = {
  fill: 'black',
  display: 'flex',
};

const IconWrapperStyle = {
  margin: 15,
  fontFamily: 'system-ui',
};

const IconStyle = {
  width: 50,
  height: 50,
  margin: 5,
};

const LogoStyle = {
  backgroundColor: '#e62429',
  width: 130,
  height: 50,
  margin: 5,
};

const IconWrapper = ({ children }: { children: ReactElement<any, any> }) => (
  <div className="icon__wrapper" style={IconWrapperStyle}>
    <div className="icon__title">{children.type.name}</div>
    {children}
  </div>
);

export const AllIcons = () => (
  <div className="icons__list" style={IconsListStyle}>
    <IconWrapper>
      <Cancel style={IconStyle} />
    </IconWrapper>
    <IconWrapper>
      <Search style={IconStyle} />
    </IconWrapper>
    <IconWrapper>
      <Logo style={LogoStyle} />
    </IconWrapper>
  </div>
);

export default { title: 'Icons' };
