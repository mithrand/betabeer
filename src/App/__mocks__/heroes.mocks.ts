import { IHero } from '../../types';

export const heroesMocks : IHero[] = [
  {
    id: '1009718',
    name: 'Wolverine',
    description:
      "Born with super-human senses and the power to heal from almost any wound, Wolverine was captured by a secret Canadian organization and given an unbreakable skeleton and claws. Treated like an animal, it took years for him to control himself. Now, he's a premiere member of both the X-Men and the Avengers.",
    thumbnail: 'http://i.annihil.us/u/prod/marvel/i/mg/2/60/537bcaef0f6cf.jpg',
  },
  {
    id: '1017297',
    name: 'Wolverine (LEGO Marvel Super Heroes)',
    description: '',
    thumbnail: 'http://i.annihil.us/u/prod/marvel/i/mg/6/00/5239c3b29cb40.jpg',
  },
  {
    id: '1017325',
    name: 'Wolverine (Marvel War of Heroes)',
    description: '',
    thumbnail: 'http://i.annihil.us/u/prod/marvel/i/mg/1/60/5239bf9387906.jpg',
  },
  {
    id: '1011006',
    name: 'Wolverine (Ultimate)',
    description:
      'Decades after participating in military airdrops with Captain America during WWII, James Howlett was abducted and experimented upon by a covert government unit, who bonded unbreakable adamantium to his skeleton, implanted three claws in each arm, dubbed him Weapon X and supposedly programmed him to kill any human he came into contact with.',
    thumbnail: 'http://i.annihil.us/u/prod/marvel/i/mg/9/03/531773b76840c.jpg',
  },
  {
    id: '1017479',
    name: 'Wolverine (X-Men: Battle of the Atom)',
    description: '',
    thumbnail: 'http://i.annihil.us/u/prod/marvel/i/mg/6/90/52d72b4c8376c.jpg',
  },
];
