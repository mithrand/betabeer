import React from 'react';
import './App.styles.scss';
import Header from '../components/Header';
import Content from '../components/Content';
import Footer from '../components/Footer';

import { heroesMocks } from './__mocks__/heroes.mocks';

export const App: React.FC = () => (
  <div className="app">
    <Header />
    <Content heroes={heroesMocks} lodading={false} />
    <Footer />
  </div>
);
