export interface IHero {
    id: string;
    name: string;
    description: string;
    thumbnail: string;
}