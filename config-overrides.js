/* config-overrides.js */

module.exports = function override(config, env) {
    config.plugins.push('emotion');
    return config;
  }